# Acamica - Sprint Project

## Backend Delilah Restó

API to manage all Delilah restaurant orders.

## Tech Stack 🛠️

**Server:** NodeJS, Express, Swagger, Sequelize, JWT, Mocha & Chai

## Run Locally 🔧

Clone the project

```bash
  git clone https://gitlab.com/matiasdamelio/acamica-sprint-project-1.git
```

Go to the project directory

```bash
  cd acamica-sprint-project-1
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm start
```

## Environment Variables ⚙️

To run this project, you will need to add the following environment variables to your .env file

`PORT`
`SECRET_KEY`

## Documentation 📖

[Swagger Documentation](http://127.0.0.1:3000/docs/)

## Database

This project uses SQLite as the SQL database engine. Therefore it is not necessary to install any additional database. The file in which the database is located is the following:

```
src/db/database.sqlite
```

The database comes already preloaded with sample records for all the entities used by the project.

## Entity Relationship Diagram

![ERD](images/der.png)

## Usage/Examples 🚀

**To run endpoints that require access use the following credentials:**

Administrator credentials

```
admin : admin
```

Test credentials

```
test : test
```

## Testing

**After start server, to run test:**

```
npm test
```

Result

![TEST](images/test.PNG)

## Authors ✒️

- **Matias D'Amelio** - [matiasdamelio](https://www.gitlab.com/matiasdamelio)

---

⌨️ con ❤️ por [matiasdamelio](https://gitlab.com/matiasdamelio)
