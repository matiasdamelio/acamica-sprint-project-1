const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    const PaymentMethod = sequelize.define("payment_methods", {
        name: {
            type: DataTypes.STRING
        }
    });

    return PaymentMethod;
};