const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    const OrderProducts = sequelize.define("order_products", {
        name: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.FLOAT
        },
        quantity: {
            type: DataTypes.INTEGER
        }
    });

    return OrderProducts;
};