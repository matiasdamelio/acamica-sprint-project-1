const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    const User = sequelize.define("users", {
        username: {
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING
        },
        fullname: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING
        },
        phone: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.STRING
        },
        isAdmin: {
            type: DataTypes.BOOLEAN
        },
        isActive: {
            type: DataTypes.BOOLEAN
        }
    });

    return User;
};