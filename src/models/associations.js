module.exports = (db) => {

    db.paymentmethod.hasOne(db.order);
    db.order.belongsTo(db.paymentmethod);

    db.user.hasOne(db.order);
    db.order.belongsTo(db.user);

    db.order.belongsToMany(db.product, {
        through: db.order_products,
        foreignKey: "orderId"
    });
    db.product.belongsToMany(db.order, {
        through: db.order_products,
        foreignKey: "productId"
    });
};
