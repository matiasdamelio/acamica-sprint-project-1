const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    const Order = sequelize.define("orders", {
        status: {
            type: DataTypes.STRING
        },
        deliveryAddress: {
            type: DataTypes.STRING
        },
        total: {
            type: DataTypes.FLOAT
        }
    });

    return Order;
};