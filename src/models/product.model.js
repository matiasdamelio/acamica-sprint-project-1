const { DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    const Product = sequelize.define("products", {
        name: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.FLOAT
        }
    });

    return Product;
};
