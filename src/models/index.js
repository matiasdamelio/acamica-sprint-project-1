const config = require('../config/db.config');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(
    {
        dialect: config.dialect,
        pool: {
            max: config.pool.max,
            min: config.pool.min,
            acquire: config.pool.acquire,
            idle: config.pool.idle
        },
        storage: config.storage
    }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.paymentmethod = require('./payment.model')(sequelize);
db.product = require('./product.model')(sequelize);
db.user = require('./user.model')(sequelize);
db.order = require('./order.model')(sequelize);
db.order_products = require('./orderproducts.model')(sequelize);

require('./associations')(db);

module.exports = db;