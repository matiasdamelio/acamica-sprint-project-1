const db = require("../models");
const Order = db.order;
const Product = db.product;
const paymentController = require('../controllers/payment.controller');
const Sequelize = require('sequelize');
const {calcTotal} = require('../utils/utils')

const statusPossible = ["Confirmed", "In Process", "Sent", "Delivered"];
const statusAdmin = ["In Process", "Sent", "Delivered"];

const createOrder = async (userId, newOrder) => {
    return Order.create({
        status: 'Pending',
        deliveryAddress: newOrder.deliveryAddress,
        total: newOrder.total,
        userId: userId,
        paymentMethodId: await paymentController.getPaymentMethodID(newOrder.paymentMethod)
    })
        .then((order) => { return order })
        .catch(err => {
            throw new Error(err.message);
        });

};

const addProduct = async (orderId, product, quantity,) => {
    return Order.findByPk(orderId)
        .then((order) => {
            if (!order) {
                res.status(405).send({ message: `Order not found` });
            }
            return Product.findByPk(product.id).then((prod) => {
                if (!prod) {
                    throw new Error('Product not found');
                }

                order.addProduct(prod, {
                    through: {
                        name: product.name,
                        price: product.price,
                        quantity: quantity
                    }
                });
                return order;
            });
        })
        .catch((err) => {
            throw new Error(err.message);
        });
};

const getOrdersByUser = async (userId) => {
    return Order.findAll({
        where: { userId: userId },
        include: [{
            model: Product,
            attributes: ['id',
                [Sequelize.literal('"products->order_products".name'), 'name'],
                [Sequelize.literal('"products->order_products".price'), 'price'],
                [Sequelize.literal('"products->order_products".quantity'), 'quantity']],
            through: { attributes: [] }
        }]
    })
        .then((orders) => { return orders });
};

const getOrdersByID = async (orderId) => {
    return Order.findOne({
        where: { id: orderId },
        include: [{
            model: Product,
            attributes: ['id',
                [Sequelize.literal('"products->order_products".name'), 'name'],
                [Sequelize.literal('"products->order_products".price'), 'price'],
                [Sequelize.literal('"products->order_products".quantity'), 'quantity']],
            through: { attributes: [] }
        }]
    })
        .then((order) => { return order });
};

const getOrders = async () => {
    return Order.findAll({
        include: [{
            model: Product,
            attributes: ['id',
                [Sequelize.literal('"products->order_products".name'), 'name'],
                [Sequelize.literal('"products->order_products".price'), 'price'],
                [Sequelize.literal('"products->order_products".quantity'), 'quantity']],
            through: { attributes: [] }
        }]
    })
        .then((orders) => { return orders });
};

const changeStatus = async (orderId, newStatus) => {
    return Order.findByPk(orderId)
        .then((order) => {
            if (statusAdmin.includes(newStatus) && statusPossible.includes(order.status) && order.status != newStatus) {
                return Order.update({
                    status: newStatus
                }, { where: { id: orderId } })
                    .then((order) => { return true });
            }
            return false;
        })
};

const closeOrder = async (orderId) => {
    return Order.update({
        status: 'Confirmed'
    }, { where: { id: orderId } })
        .then((order) => { return order });
};

const updateTotal = async (orderId) => {
    return getOrdersByID(orderId)
        .then((order) => {
            return Order.update({
                total: calcTotal(order.products)
            }, { where: { id: orderId } })
                .then((order) => { return order });
        });
};

const deleteProduct = async (orderId, productId) => {
    return Order.findByPk(orderId)
        .then((order) => {
            order.removeProduct([productId]);
            return order;
        })
        .catch((err) => {
            throw new Error(err.message);
        });
};

const editProduct = async (orderId, productId, newQuantity) => {
    return Order.findByPk(orderId)
        .then((order) => {
            return Product.findByPk(productId).then((prod) => {
                order.addProducts(prod, {
                    through: {
                        quantity: newQuantity
                    }
                });
                return order;
            });
        });
};

module.exports = {
    createOrder,
    addProduct,
    getOrdersByUser,
    getOrdersByID,
    getOrders,
    changeStatus,
    closeOrder,
    updateTotal,
    deleteProduct,
    editProduct
}