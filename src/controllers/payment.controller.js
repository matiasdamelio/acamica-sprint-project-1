const db = require("../models");
const PaymentMethod = db.paymentmethod;

const createPaymentMethod = (newPayment) => {
    return PaymentMethod.create({
        name: newPayment.name
    })
        .then((paymentMethod) => { return paymentMethod })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

const getPaymentMethods = () => {
    return PaymentMethod.findAll()
        .then((paymentMethods) => { return paymentMethods });
};

const getPaymentMethod = (name) => {
    return PaymentMethod.findOne({ where: { name: name } })
        .then((paymentMethod) => { return paymentMethod });
};

const getPaymentMethodID = (name) => {
    return PaymentMethod.findOne({ where: { name: name } })
        .then((paymentMethod) => { return paymentMethod.id });
};

const updatePaymentMethod = (id, newPayment) => {
    return PaymentMethod.update({
        name: newPayment.name
    }, { where: { id } })
        .then((paymentMethod) => { return paymentMethod });
};

const deletePaymentMethod = (id) => {
    return PaymentMethod.destroy({
        where: { id: id }
    });
};

module.exports = {
    getPaymentMethods,
    getPaymentMethod,
    getPaymentMethodID,
    createPaymentMethod,
    updatePaymentMethod,
    deletePaymentMethod
};