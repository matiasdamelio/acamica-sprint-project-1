const db = require("../models");
const Product = db.product;
const {convertPrice} = require('../utils/utils')

const createProduct = (newProduct) => {
    return Product.create({
        name: newProduct.name,
        price: convertPrice(newProduct.price)
    })
        .then((product) => { return product })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

const getProducts = () => {
    return Product.findAll()
        .then((products) => { return products });
};

const getProduct = (name) => {
    return Product.findOne({ where: { name: name } })
        .then((product) => { return product });
};

const updateProduct = (id, newProduct) => {
    return Product.update({
        name: newProduct.name,
        price: convertPrice(newProduct.price)
    }, { where: { id } })
        .then((product) => { return product });
};

const deleteProduct = (id) => {
    return Product.destroy({
        where: { id: id }
    });
};

module.exports = {
    getProducts,
    getProduct,
    createProduct,
    updateProduct,
    deleteProduct
};