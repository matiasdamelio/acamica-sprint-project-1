const db = require("../models");
const User = db.user;

const bcrypt = require('bcryptjs');

const createUser = (newUser) => {
    return User.create({
        username: newUser.username,
        password: bcrypt.hashSync(newUser.password, 8),
        fullname: newUser.fullname,
        email: newUser.email,
        phone: newUser.phone,
        address: newUser.address,
        isAdmin: false,
        isActive: true
    })
        .then((user) => { return user })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

const getUsers = () => {
    return User.findAll()
        .then((users) => { return users });
};

const getUser = (username) => {
    return User.findOne({ where: { username: username } })
        .then((user) => { return user });
};

const getUserID = (username) => {
    return User.findOne({ where: { username: username } })
        .then((user) => { return user.id });
};

const getAddressByID = (userId) => {
    return User.findByPk(userId)
        .then((user) => { return user.address });
};

const suspendUser = (id) => {
    return User.update({
        isActive: false
    }, { where: { id } })
        .then((user) => { return user });
};

module.exports = {
    getUsers,
    getUser,
    getUserID,
    createUser,
    getAddressByID,
    suspendUser
};