require('dotenv').config();
const PORT = process.env.PORT || 3000;

const express = require('express');
const compression = require('compression');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const helmet = require('helmet');

const logger = require('./utils/logger');
const { swaggerOptions } = require('./utils/swagger.options')

const db = require('./models');

const app = express();

app.use(express.json());
app.use(compression());
app.use(helmet());

app.use('/docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerJsDoc(swaggerOptions)));

app.use(logger);

(async () => {
    await db.sequelize.sync();
    // await db.sequelize.sync({ force: true });
    // console.info('Drop and Resync DB');
    // require('./utils/initial')();
})();

app.get('/', (req, res) => {
    res.send('Acamica Sprint Projects - My APP Persistente')
})

app.use('/signup', require('./routes/signup.routes'));
app.use('/users', require('./routes/user.routes'));
app.use('/products', require('./routes/products.routes'));
app.use('/payments', require('./routes/payments.routes'));
app.use('/orders', require('./routes/orders.routes'));

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});