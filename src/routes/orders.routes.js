const express = require('express');
const router = express.Router();
const orderController = require('../controllers/order.controller')
const { verifyToken, isAdmin, isActive } = require('../middlewares/auth.middleware');
const { checkFields, checkProducts, checkQuantity, mustBeInteger, mustBeInOrders, 
    orderOwner, stillOpen, productInProducts, productNotInProducts, checkOneProduct } = require('../middlewares/order.middleware');

// ADMIN ROUTES

/**
 * @swagger
 * /orders/all_orders:
 *   get:
 *      summary: Retrieve a list of all orders
 *      tags: [Orders]
 *      description: Retrieve a list of all orders in Store.
 *      responses:
 *          200:
 *              description: The list of orders
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/OrderResponse'
 *          205:
 *              $ref: '#/components/responses/205'
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.get('/all_orders', verifyToken, isAdmin, async (req, res) => {
    const orders = await orderController.getOrders();
    if (orders.length === 0) {
        return res.status(205).json({ message: 'No orders available.' })
    }
    res.json(orders);
});

/**
 * @swagger
 * /orders/change_status/{id}/{status}:
 *  put:
 *      summary: Update status order
 *      tags: [Orders]
 *      description: Update status of an order.
 *      parameters:
 *          -   $ref: '#/components/parameters/orderParam'
 *          -   $ref: '#/components/parameters/statusParam'
 *      responses:
 *          200:
 *              description: The status order was successfully updated
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The order \#{id} has been updated
 *          400:
 *              description: The new status is invalid
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The new status is invalid. or ID must be an integer.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 *          404:
 *              description: Order does not exist
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message: 
 *                              type: string
 *                          exacmple: Order does not exist
 */
router.put('/change_status/:id/:status', verifyToken, isAdmin, mustBeInteger, mustBeInOrders, async (req, res) => {
    const change = await orderController.changeStatus(req.params.id, req.params.status);
    if (change) {
        res.json({ message: `The order #${req.params.id} has been updated` });
    } else {
        res.status(400).json({ message: `The new status is invalid.` });
    }
})

// USER ROUTES

/**
 * @swagger
 * /orders/:
 *   get:
 *      summary: Retrieve a list of orders by username 
 *      tags: [Orders]
 *      description: Retrieve a list of orders in Store by username.
 *      responses:
 *          200:
 *              description: The list of orders
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/OrderResponse'
 *          205:
 *              $ref: '#/components/responses/205'
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.get('', verifyToken, async (req, res) => {
    const orders = await orderController.getOrdersByUser(req.userId);
    if (orders.length === 0) {
        return res.status(205).json({ message: 'No orders available.' })
    }
    res.json(orders);
});

/**
 * @swagger
 * /orders/new:
 *  post:
 *      summary: New order
 *      tags: [Orders]
 *      description: Creation of a new order.
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Order' 
 *      responses:
 *          201:
 *              description: The order was successfully created
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The order has been created
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: One of the fields is empty or missing. or Product does not exist. or Quantity must be numeric and greater than 0. or No products were received.
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.post('/new', verifyToken, isActive, checkFields, checkProducts, async (req, res) => {
    const order = await orderController.createOrder(req.userId, req.body);
    for await (const product of req.body.products) {
        await orderController.addProduct(order.id, product.product, product.quantity);
    };
    res.status(201).json({ message: `The order has been created` });
});

/**
 * @swagger
 * /orders/edit/{id}/{product_id}:
 *  put:
 *      summary: Edit the quantity of a product in an order 
 *      tags: [Orders]
 *      description: Edit the quantity of a product in an order.
 *      parameters:
 *          -   $ref: '#/components/parameters/orderParam'
 *          -   $ref: '#/components/parameters/orderProductParam'
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - quantity
 *                      properties:
 *                          quantity:
 *                              type: integer
 *                              description: Quantity of the product
 *                      example:    
 *                          quantity: 5
 *      responses:
 *          200:
 *              description: The product was successfully updated
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The quantity product \#{product_id} in order \#{id} has been updated
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or Order does not exist. or You are not the owner of this order. Product does not exist in the order. 
 *                                      or The order is already confirmed. Quantity must be numeric and greater than 0. or Quantity must be complete.
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.put('/edit/:id/:product_id', verifyToken, mustBeInteger, mustBeInOrders, orderOwner, stillOpen, productInProducts, checkQuantity, async (req, res) => {
    const order = await orderController.editProduct(req.params.id, req.params.product_id, req.body.quantity);
    await order.reload();
    await orderController.updateTotal(req.params.id);
    res.json({ message: `The quantity product #${req.params.product_id} in order #${req.params.id} has been updated` })
});

/**
 * @swagger
 * /orders/{id}/{product_id}:
 *  delete:
 *      summary: Delete a product from an order 
 *      tags: [Orders]
 *      description: Delete a product from an order.
 *      parameters:
 *          -   $ref: '#/components/parameters/orderParam'
 *          -   $ref: '#/components/parameters/orderProductParam'
 *      responses:
 *          200:
 *              description: The product was successfully deleted
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The product \#{product_id} in order \#{id} has been deleted
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or Order does not exist. or You are not the owner of this order. Product does not exist in the order. or The order is already confirmed.
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.delete('/:id/:product_id', verifyToken, mustBeInteger, mustBeInOrders, orderOwner, isActive, stillOpen, productInProducts, async (req, res) => {
    const order = await orderController.deleteProduct(req.params.id, req.params.product_id);
    await order.reload();
    await orderController.updateTotal(req.params.id);
    res.json({ message: `The product #${req.params.product_id} in order #${req.params.id} has been deleted` })
});

/**
 * @swagger
 * /orders/add/{id}/{product_id}:
 *  put:
 *      summary: Add a new product to an existing order
 *      tags: [Orders]
 *      description: Add a new product to an existing order.
 *      parameters:
 *          -   $ref: '#/components/parameters/orderParam'
 *          -   $ref: '#/components/parameters/orderProductParam'
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      required:
 *                          - quantity
 *                      properties:
 *                          quantity:
 *                              type: integer
 *                              description: Quantity of the product
 *                      example:    
 *                          quantity: 5
 *      responses:
 *          200:
 *              description: The product was successfully added
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The product \#{product_id} has been added in order \#{id}
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or Order does not exist. or You are not the owner of this order. Product already exist in the order. 
 *                                      or The order is already confirmed. Quantity must be numeric and greater than 0. or Quantity must be complete. or Product does not exist.
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.put('/add/:id/:product_id', verifyToken, mustBeInteger, mustBeInOrders, orderOwner, isActive, stillOpen, productNotInProducts, checkQuantity, checkOneProduct, async (req, res) => {
    const order = await orderController.addProduct(req.params.id, req.body.products.product, req.body.products.quantity);
    await order.reload();
    await orderController.updateTotal(req.params.id);
    res.json({ message: `The product #${req.params.product_id} has been added in order #${req.params.id}` })
}); 

/**
 * @swagger
 * /orders/close/{id}:
 *  put:
 *      summary: Close order
 *      tags: [Orders]
 *      description: Close order.
 *      parameters:
 *          -   $ref: '#/components/parameters/orderParam'
 *      responses:
 *          200:
 *              description: The order was successfully closed
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The order \#{id} has been closed
 *          400:
 *              description: The order is already confirmed
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The order #{id} is already confirmed. or You are not the owner of this order. or ID must be an integer.
 *          401:
 *              $ref: '#/components/responses/401'
 *          404:
 *              description: Order does not exist
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message: 
 *                              type: string
 *                          exacmple: Order does not exist
 */
router.put('/close/:id', verifyToken, mustBeInteger, mustBeInOrders, orderOwner, isActive, stillOpen, async (req, res) => {
    await orderController.closeOrder(req.params.id);
    res.json({ message: `The order #${req.params.id} has been closed` });
});

module.exports = router;