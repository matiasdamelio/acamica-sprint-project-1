const express = require('express');
const router = express.Router();
const productController = require('../controllers/product.controller')
const { checkFields, checkFieldsUpdate, mustBeInteger, mustBeInProducts } = require('../middlewares/product.middleware');
const { verifyToken, isAdmin } = require('../middlewares/auth.middleware');

/**
 * @swagger
 * /products/:
 *   get:
 *      summary: Retrieve a list of products
 *      tags: [Products]
 *      description: Retrieve a list of products available in Store.
 *      responses:
 *          200:
 *              description: The list of products 
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/ProductResponse'
 *          205:
 *              $ref: '#/components/responses/205'
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.get('/', async (req, res) => {
    const products = await productController.getProducts();
    if (products.length === 0) {
        res.status(205).json({ message: 'No products available.' })
    }
    res.json(products);
});

/**
 * @swagger
 * /products/:
 *  post:
 *      summary: New product
 *      tags: [Products]
 *      description: Loading of a new product.
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Product' 
 *      responses:
 *          201:
 *              description: The product was successfully loaded
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The product {name} has been created
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: Price must be numeric and greater than 0. or Product already exists. or One of the fields is empty or missing.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.post('/', verifyToken, isAdmin, checkFields, async (req, res) => {
    await productController.createProduct(req.body);
    res.status(201).json({ message: `The product '${req.body.name}' has been created` });
});

/**
 * @swagger
 * /products/{id}:
 *  put:
 *      summary: Update product
 *      tags: [Products]
 *      description: Update of a product.
 *      parameters:
 *          -   $ref: '#/components/parameters/productParam'
 *      requestBody:
 *          required: false
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Product' 
 *      responses:
 *          200:
 *              description: The product was successfully updated
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The product \#{id} has been updated
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or Product does not exist. or Product name already exists. or Price must be numeric and greater than 0. or At least one field must be complete.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.put('/:id', verifyToken, isAdmin, mustBeInteger, mustBeInProducts, checkFieldsUpdate, async (req, res) => {
    await productController.updateProduct(req.params.id, req.body);
    res.json({ message: `The product #${req.params.id} has been updated` });
});

/**
 * @swagger
 * /products/{id}:
 *  delete:
 *      summary: Delete product
 *      tags: [Products]
 *      description: Delete a product.
 *      parameters:
 *          -   $ref: '#/components/parameters/productParam'
 *      responses:
 *          200:
 *              description: The product was successfully deleted
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The product \#{id} has been deleted
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or Product does not exist.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.delete('/:id', verifyToken, isAdmin, mustBeInteger, mustBeInProducts, async (req, res) => {
    await productController.deleteProduct(req.params.id)
    res.json({ message: `The product #${req.params.id} has been deleted` })
})

module.exports = router;