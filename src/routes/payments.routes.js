const express = require('express');
const router = express.Router();
const paymentController = require('../controllers/payment.controller')
const { checkFields, checkFieldsUpdate, mustBeInteger, mustBeInPayments } = require('../middlewares/payment.middleware');
const { verifyToken, isAdmin } = require('../middlewares/auth.middleware');

/**
 * @swagger
 * /payments/:
 *   get:
 *      summary: Retrieve a list of payment methods
 *      tags: [Payment Methods]
 *      description: Retrieve a list of payment methods available in Store.
 *      responses:
 *          200:
 *              description: The list of payment methods 
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/PaymentResponse'
 *          205:
 *              $ref: '#/components/responses/205'
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.get('/', verifyToken, isAdmin, async (req, res) => {
    // payments = Payments.getPaymentMethods();
    const payments = await paymentController.getPaymentMethods();
    if (payments.length === 0) {
        res.status(205).json({ message: 'No payment methods available.' })
    }
    res.json(payments);
});

/**
 * @swagger
 * /payments/:
 *  post:
 *      summary: New payment method
 *      tags: [Payment Methods]
 *      description: Creation of a new payment method.
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Payment' 
 *      responses:
 *          201:
 *              description: The payment method was successfully created
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The payment method {name} has been created
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: Payment method already exists. or Name field is empty or missing.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.post('/', verifyToken, isAdmin, checkFields, async (req, res) => {
    await paymentController.createPaymentMethod(req.body);
    res.status(201).json({ message: `The payment method '${req.body.name}' has been created` });
});

/**
 * @swagger
 * /payments/{id}:
 *  put:
 *      summary: Update payment method
 *      tags: [Payment Methods]
 *      description: Update of a payment method.
 *      parameters:
 *          -   $ref: '#/components/parameters/paymentParam'
 *      requestBody:
 *          required: false
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Payment' 
 *      responses:
 *          200:
 *              description: The payment method was successfully updated
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The payment method \#{id} has been updated
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or Payment method does not exist. or Payment method name already exists. or Name must be complete.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.put('/:id', verifyToken, isAdmin, mustBeInteger, mustBeInPayments, checkFieldsUpdate, async (req, res) => {
    await paymentController.updatePaymentMethod(req.params.id, req.body);
    res.json({ message: `The payment method #${req.params.id} has been updated` });
});

/**
 * @swagger
 * /payments/{id}:
 *  delete:
 *      summary: Delete payment method
 *      tags: [Payment Methods]
 *      description: Delete a payment method.
 *      parameters:
 *          -   $ref: '#/components/parameters/paymentParam'
 *      responses:
 *          200:
 *              description: The payment method was successfully deleted
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The payment method \#{id} has been deleted
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or Payment method does not exist.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.delete('/:id', verifyToken, isAdmin, mustBeInteger, mustBeInPayments, async (req, res) => {
    await paymentController.deletePaymentMethod(req.params.id)
    res.json({ message: `The payment method #${req.params.id} has been deleted` })
})

module.exports = router;