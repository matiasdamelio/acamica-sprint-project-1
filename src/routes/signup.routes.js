const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller')
const {checkFieldsRegister} = require('../middlewares/user.middleware');

/**
 * @swagger
 * /signup:
 *  post:
 *      summary: New user registration
 *      tags: [Sign Up]
 *      security: []
 *      description: New user registration.
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/User' 
 *      responses:
 *          201:
 *              description: The user was successfully registered
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The user {user} has been created
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: User already exists. or One of the fields is empty or missing.
 */
router.post('/', checkFieldsRegister, (req, res) => {
    userController.createUser(req.body);
    res.status(201).json({ message: `The user '${req.body.username}' has been created` });
});

module.exports = router;