const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller')
const config = require('../config/auth.config');
const { verifyToken, isAdmin } = require('../middlewares/auth.middleware');
const {mustBeInteger, mustBeInUsers} = require('../middlewares/user.middleware');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

/**
 * @swagger
 * /users/login:
 *  post:
 *      summary: Login 
 *      tags: [Users]
 *      security: []
 *      description: Login.
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/UserLogin' 
 *      responses:
 *          200:
 *              description: Successfully logged in
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: You have successfully logged in with user {user}
 *                               
 *          401:
 *              $ref: '#/components/responses/401'
 */
router.post('/login', (req, res) => {
    const { username, password } = req.body;

    if (username && password) {
        userController.getUser(req.body.username)
            .then(user => {
                if (!user) {
                    return res.status(404).json({ message: "User not found." });
                }

                if (bcrypt.compareSync(password, user.password)) {
                    const token = jwt.sign({
                        id: user.id,
                        username: user.username
                    }, config.secret, { expiresIn: 3600 });

                    return res.status(200).json({
                        id: user.id,
                        username: user.username,
                        email: user.email,
                        isAdmin: user.isAdmin,
                        accessToken: token
                    });
                }

                res.status(404).json({ message: "Invalid user/password" })
            })
            .catch(err => {
                res.status(500).json({ message: err.message })
            });
    } else {
        res.status(400).json({ message: 'One of the fields is empty or missing.' })
    }
});

/**
 * @swagger
 * /users:
 *   get:
 *      summary: Retrieve a list of users
 *      tags: [Users]
 *      description: Retrieve a list of users.
 *      responses:
 *          200:
 *              description: The list of users 
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/UserResponse'
 *          205:
 *              $ref: '#/components/responses/205'
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.get('/', verifyToken, isAdmin, async (req, res) => {
    const users = await userController.getUsers();
    if (users.length === 0) {
        res.status(205).json({ message: 'No users available.' })
    }
    res.json(users);
});

/**
 * @swagger
 * /users/suspend/{id}:
 *  put:
 *      summary: Suspend user
 *      tags: [Users]
 *      description: Suspend an user.
 *      parameters:
 *          -   $ref: '#/components/parameters/userParam'
 *      responses:
 *          200:
 *              description: The user was successfully suspended
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The user \#{id} has been suspended
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or User does not exist.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.put('/suspend/:id', verifyToken, isAdmin, mustBeInteger, mustBeInUsers, async(req, res) => {
    await userController.suspendUser(req.params.id);
    res.json({ message: `The user #${req.params.id} has been suspended` });
});

/**
 * @swagger
 * /users/activate/{id}:
 *  put:
 *      summary: Activate user
 *      tags: [Users]
 *      description: Activate an user.
 *      parameters:
 *          -   $ref: '#/components/parameters/userParam'
 *      responses:
 *          200:
 *              description: The user was successfully activated
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: The user \#{id} has been activated
 *          400:
 *              description: Error in check fields    
 *              content: 
 *                  application/json:
 *                      schema:
 *                          type: object
 *                      properties:
 *                          message:
 *                              type: string  
 *                      example: 
 *                          message: ID must be an integer. or User does not exist.
 *          401:
 *              $ref: '#/components/responses/401'
 *          403:
 *              $ref: '#/components/responses/403'
 */
router.put('/activate/:id', verifyToken, isAdmin, mustBeInteger, mustBeInUsers, async(req, res) => {
    await userController.suspendUser(req.params.id);
    res.json({ message: `The user #${req.params.id} has been activated` });
});

module.exports = router;