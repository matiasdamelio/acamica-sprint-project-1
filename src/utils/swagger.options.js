exports.swaggerOptions = {
    definition: {
        openapi: '3.0.1',
        info: {
            title: 'Delilah Restó',
            description: 'API to manage all Delilah restaurant orders.',
            version: '2.0.0',
            contact: {
                name: "Matias D'Amelio",
                email: "matiasdamelio@gmail.com"
            }
        },
        servers: [
            {
                url: 'http://localhost:3000',
                description: 'Development server',
            },
        ],
        components: {
            securitySchemes: {
                jwt: {
                    type: "http",
                    scheme: "bearer",
                    in: "header",
                    bearerFormat: "JWT"
                }
            }
        },
        security: [
            {
                jwt: []
            }
        ]
    },
    swaggerUiOptions: {
        persistAuthorization: true,
    },
    apis: [
        './src/utils/swagger.components.js',
        './src/routes/*.js',
    ]
};




