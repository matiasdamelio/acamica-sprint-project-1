const bcrypt = require('bcryptjs');

// Data Json
const users = require('../data/users.json');
const products = require('../data/products.json');
const payment_methods = require('../data/payment_methods.json')
const orders = require('../data/orders.json')

// DB - Models
const db = require('../models');
const User = db.user;
const Product = db.product;
const PaymentMethod = db.paymentmethod;
const Order = db.order;

// Controllers
const orderController = require('../controllers/order.controller');

module.exports = function () {

    users.forEach(user => User.create({
        username: user.username,
        password: bcrypt.hashSync(user.password, 8),
        fullname: user.fullname,
        email: user.email,
        phone: user.phone,
        address: user.address,
        isAdmin: user.isAdmin,
        isActive: user.isActive
    }));

    products.forEach(product => Product.create({
        name: product.name,
        price: product.price
    }));

    payment_methods.forEach(payment_method => PaymentMethod.create({
        name: payment_method.name
    }));

    (async function () {
        for await (const order of orders) {
            const ord = await Order.create({
                status: order.status,
                deliveryAddress: order.deliveryAddress,
                total: order.total,
                userId: order.userID,
                paymentMethodId: order.paymentMethodID
            });
            for (const product of order.products) {
                await orderController.addProduct(ord.id, product.product, product.quantity);
            };
        };
    })();
};

