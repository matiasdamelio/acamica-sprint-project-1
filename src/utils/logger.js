const logger = (req, res, next) => {
    const date = new Date().toLocaleString()
    console.log(`[${date}] : URL: ${req.url} - METHOD: ${req.method} - PATH: ${req.path}`);
    next();
};

module.exports = logger;