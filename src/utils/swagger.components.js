/**
 * @swagger
 * tags:
 *  -   name: Sign Up
 *      description: Registration section
 *  -   name: Users
 *      description: User section
 *  -   name: Products
 *      description: Products section
 *  -   name: Payment Methods
 *      description: Payment Methods Section
 *  -   name: Orders
 *      description: Orders Section
 *
 * components:
 *  parameters:
 *      userParam:
 *          name: id
 *          in: path
 *          description: ID of user
 *          required: true
 *          schema:
 *              type: integer
 *              example: 10
 *      productParam:
 *          name: id
 *          in: path
 *          description: ID of product
 *          required: true
 *          schema:
 *              type: integer
 *              example: 10
 *      paymentParam:
 *          name: id
 *          in: path
 *          description: ID of payment method
 *          required: true
 *          schema:
 *              type: integer
 *              example: 5
 *      orderParam:
 *          name: id
 *          in: path
 *          description: ID of order
 *          required: true
 *          schema:
 *              type: integer
 *              example: 3
 *      orderProductParam:
 *          name: product_id
 *          in: path
 *          description: ID of product in order
 *          required: true
 *          schema:
 *              type: integer
 *              example: 5
 *      statusParam:
 *          name: status
 *          in: path
 *          description: Status order
 *          required: true
 *          schema:
 *              type: string
 *              enum: [Confirmed, In Process, Sent, Delivered]
 *  responses:
 *
 *      401:
 *          description: Unauthorized - No token provided
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          message:
 *                              type: string
 *                      example:
 *                          message: No token provided
 *
 *      403:
 *          description: Forbidden - Administrator permissions
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          message:
 *                              type: string
 *                      example:
 *                          message: You're not an admin
 *      205:
 *          description: No data available
 *          content:
 *              application:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          message:
 *                              type: string
 *                      example:
 *                          message: No data available
 *  schemas:
 *      User:
 *          type: object
 *          required:
 *              - username
 *              - password
 *              - fullname
 *              - email
 *              - phone
 *              - address
 *          properties:
 *              username:
 *                  type: string
 *                  description: The user name
 *              password:
 *                  type: string
 *                  description: The user password
 *              fullname:
 *                  type: string
 *                  description: The user full name
 *              email:
 *                  type: string
 *                  description: The user email
 *              phone:
 *                  type: string
 *                  description: The user phone number
 *              address:
 *                  type: string
 *                  description: The user address
 *          example:
 *              username: test
 *              password: test
 *              fullname: Test Test
 *              email: test@test.com
 *              phone: +54 (222) 222-2222
 *              address: 2222 Test Street
 *      UserLogin:
 *          type: object
 *          required:
 *              - username
 *              - password
 *          properties:
 *              username:
 *                  type: string
 *                  description: The user name
 *              password:
 *                  type: string
 *                  description: The user password
 *          example:
 *              username: test
 *              password: test
 *      UserResponse:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  description: The user ID
 *              username:
 *                  type: string
 *                  description: The user name
 *              password:
 *                  type: string
 *                  description: The user password
 *              fullname:
 *                  type: string
 *                  description: The user full name
 *              email:
 *                  type: string
 *                  description: The user email
 *              phone:
 *                  type: string
 *                  description: The user phone number
 *              address:
 *                  type: string
 *                  description: The user address
 *              isAdmin:
 *                  type: boolean
 *                  description: True if user is admin
 *              isActive:
 *                  type: boolean
 *                  description: True if user is active
 *          example:
 *              id: 22
 *              username: test
 *              password: test
 *              fullname: Test Test
 *              email: test@test.com
 *              phone: +54 (222) 222-2222
 *              address: 2222 Test Street
 *              isAdmin: false
 *              isActive: true
 *      OrderProduct:
 *          type: object
 *          required:
 *              - id
 *              - quantity
 *          properties:
 *              id:
 *                  type: integer
 *                  description: The product ID
 *              quantity:
 *                  type: integer
 *                  description: Quantity of the product
 *          example:
 *              id: 5
 *              quantity: 10
 *      Product:
 *          type: object
 *          required:
 *              - name
 *              - price
 *          properties:
 *              name:
 *                  type: string
 *                  description: The product name
 *              price:
 *                  type: integer
 *                  description: The product price
 *          example:
 *              name: Bread
 *              price: 41.5
 *      ProductResponse:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  description: The product ID
 *              name:
 *                  type: string
 *                  description: The product name
 *              price:
 *                  type: integer
 *                  description: The product price
 *          example:
 *              id: 5
 *              name: Bread
 *              price: 41.5
 *      Payment:
 *          type: object
 *          required:
 *              - name
 *          properties:
 *              name:
 *                  type: string
 *                  description: The payment method name
 *          example:
 *              name: Efectivo
 *      PaymentResponse:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  description: The payment method ID
 *              name:
 *                  type: string
 *                  description: The payment method name
 *          example:
 *              id: 1
 *              name: Efectivo
 *      Order:
 *          type: object
 *          required:
 *              - paymentMethod
 *              - products
 *          properties:
 *              paymentMethod:
 *                  type: string
 *                  description: The payment method name
 *              deliveryAddress:
 *                  type: string
 *                  description: Order delivery address
 *              products:
 *                  type: array
 *                  items:
 *                      type: object
 *                      properties:
 *                          quantity:
 *                              type: integer
 *                              description: Quantity of the product
 *                          id:
 *                              type: integer
 *                              description: The product ID
 *          example:
 *              paymentMethod: Efectivo
 *              deliveryAddress: 2222 Test Street
 *              products: []
 *      OrderResponse:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  description: The order ID
 *              userID:
 *                  type: integer
 *                  description: The user ID
 *              datetime:
 *                  type: string
 *                  format: date-time
 *                  description: Datetime of order creation
 *              status:
 *                  type: string
 *                  description: Status of the order
 *              paymentMethodID:
 *                  type: integer
 *                  description: The payment method ID
 *              deliveryAddress:
 *                  type: string
 *                  description: Order delivery address
 *              products:
 *                  type: array
 *                  items:
 *                      type: object
 *                      properties:
 *                          quantity:
 *                              type: integer
 *                              description: Quantity of the product
 *                          product:
 *                              $ref: '#/components/schemas/ProductResponse'
 *          example:
 *              id: 1
 *              userID: 21
 *              datetime: 20210601-14:12:55
 *              status: Pending
 *              paymentMethodID: 3
 *              deliveryAddress: 2222 Test Street
 *              products: []
 *
 */