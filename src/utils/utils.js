const db = require('../models');
const Product = db.product;
const Order = db.order;

const validPrice = (price) => {
    return (Number.isFinite(price) && price > 0)
};

const convertPrice = (price) => {
    return Math.round(parseFloat(price) * 1e2) / 1e2
}

const calcTotal = (products) => {
    let total = 0;
    if (products.length != 0) {
        products.forEach(product => {
            total += product.get('quantity') * product.get('price');
        });
    }
    return convertPrice(total);
}

const isInteger = (id) => {
    return Number.isInteger(parseInt(id)) && parseInt(id) > 0
}

const inProducts = async (id) => {
    return await Product.findByPk(id)
}

const inOrders = async (id) => {
    return await Order.findByPk(id)
}

module.exports = {
    validPrice,
    convertPrice,
    calcTotal,
    isInteger,
    inProducts,
    inOrders
};