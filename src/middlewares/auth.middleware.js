const jwt = require('jsonwebtoken');
const config = require('../config/auth.config');
const db = require('../models');
const User = db.user;

verifyToken = (req, res, next) => {

    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if (!token) {
        return res.status(403).send({
            message: "No token provided!"
        });
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                message: "Unathorized!"
            });
        }
        req.userId = decoded.id;
        next();
    });
};

isAdmin = async (req, res, next) => {
    const user = await User.findByPk(req.userId)
    if (user && user.isAdmin === true) {
        return next();
    }
    return res.status(403).send({ message: "You're not an admin." })
};

isActive = async (req, res, next) => {
    const user = await User.findByPk(req.userId)
    if (user && user.isActive === true) {
        return next();
    }
    return res.status(403).send({ message: "You're suspended." })
};

module.exports = {
    verifyToken,
    isAdmin,
    isActive
};