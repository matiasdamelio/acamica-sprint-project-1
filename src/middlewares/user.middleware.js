const db = require('../models');
const User = db.user;
const userController = require('../controllers/user.controller');
const {isInteger} = require('../utils/utils');

async function checkFieldsRegister(req, res, next) {
    const { username, password, fullname, email, phone, address } = req.body;

    if (username && password && fullname && email && phone && address) {
        let users = await userController.getUsers();
        users = users.filter(u => u.username === username || u.email === email);
        if (users.length <= 0) {
            next()
        } else {
            res.status(400).json({ message: 'User already exists.' })
        };
    } else {
        res.status(400).json({ message: 'One of the fields is empty or missing.' })
    }
};

function mustBeInteger(req, res, next) {
    const id = req.params.id;
    if (isInteger(id)) {
        next()
    } else {
        res.status(400).json({ message: 'ID must be an integer' })
    }
};

async function mustBeInUsers(req, res, next) {
    const id = req.params.id;
    const user = await User.findByPk(id)
    if (user) {
        next()
    } else {
        res.status(404).json({ message: 'User does not exist' })
    }
};

module.exports = {
    checkFieldsRegister,
    mustBeInteger,
    mustBeInUsers
};