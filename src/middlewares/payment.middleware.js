const paymentController = require('../controllers/payment.controller')
const db = require('../models');
const PaymentMethod = db.paymentmethod;

async function checkFields(req, res, next) {
    const { name } = req.body;

    if (name) {
        const payment = await paymentController.getPaymentMethod(name);
        if (!payment) {
            next()
        } else {
            res.status(400).json({ message: 'Payment method already exists' })
        };
    } else {
        res.status(400).json({ message: 'Name field is empty or missing' })
    }
};

async function checkFieldsUpdate(req, res, next) {
    const { name } = req.body;
    const id = req.params.id;

    if (name) {
        const payment = await paymentController.getPaymentMethod(name);
        if (payment && payment.id != id) {
            return res.status(400).json({ message: 'Payment method name already exists' })
        }
        next();
    } else {
        res.status(400).json({ message: 'Name must be complete' })
    }
};

function mustBeInteger(req, res, next) {
    const id = req.params.id;
    if (!Number.isInteger(parseInt(id))) {
        res.status(400).json({ message: 'ID must be an integer' })
    } else {
        next()
    }
};

async function mustBeInPayments(req, res, next) {
    const id = req.params.id;
    const payment = await PaymentMethod.findByPk(id)
    if (payment) {
        next()
    } else {
        res.status(404).json({ message: 'Payment method does not exist' })
    }
};

module.exports = {
    checkFields,
    checkFieldsUpdate,
    mustBeInteger,
    mustBeInPayments
};