const db = require('../models');
const Product = db.product;
const Order = db.order;
const userController = require('../controllers/user.controller');
const orderController = require('../controllers/order.controller');
// const { isInteger, inProducts, inOrders } = require('../utils/utils.products');
const { convertPrice, isInteger, inProducts, inOrders } = require('../utils/utils');

async function checkFields(req, res, next) {
    const { paymentMethod, deliveryAddress, products } = req.body;

    if (paymentMethod && products) {
        if (!deliveryAddress) {
            const address = await userController.getAddressByID(req.userId);
            req.body.deliveryAddress = address;
        }
        if (products.length == 0) {
            return res.status(400).json({ message: 'No products were received' })
        }
        next()
    } else {
        res.status(400).json({ message: 'One of the fields is empty or missing' })
    }
};

const checkProduct = async (product) => {
    const { id, quantity } = product;

    if (id && quantity) {
        if (quantity > 0 && await inProducts(id)) {
            const productByID = await Product.findByPk(id);
            return result = { quantity, product: { id, name: productByID.name, price: productByID.price } }
        }
    }
    return false
}

function checkQuantity(req, res, next) {
    const { quantity } = req.body;
    if (quantity) {
        if (isInteger(quantity)) {
            next();
        } else {
            res.status(400).json({ message: 'Quantity must be numeric and greater than 0.' });
        }
    } else {
        res.status(400).json({ message: 'Quantity must be complete.' });
    }
};

async function checkProducts(req, res, next) {
    const { products } = req.body;

    const productResponse = [];
    let ok = true;
    let total = 0;

    for await (const product of products) {
        const result = await checkProduct(product);
        if (result) {
            productResponse.push(result)
            total += result.quantity * result.product.price;
        } else {
            ok = false;
            res.status(400).json({ message: 'Product does not exist. or Quantity must be numeric and greater than 0.' })
        }
    }
    if (ok) {
        req.body.products = productResponse;
        req.body.total = convertPrice(total);
        next();
    }
}

async function checkOneProduct(req, res, next) {
    const { quantity } = req.body;
    const { product_id } = req.params;

    let ok = true;
    let result;
    if (product_id && quantity) {
        if (quantity > 0 && await inProducts(product_id)) {
            const productByID = await Product.findByPk(product_id);
            result = { quantity, product: { id: product_id, name: productByID.name, price: productByID.price } };
        } else {
            ok = false;
            res.status(400).json({ message: 'Product does not exist. or Quantity must be numeric and greater than 0.' })
        }
    } else {
        ok = false;
        res.status(400).json({ message: 'One of the fields is empty or missing' })
    }
    if (ok) {
        req.body.products = result;
        next();
    }
};   

function mustBeInteger(req, res, next) {
    const id = req.params.id;
    if (isInteger(id)) {
        next()
    } else {
        res.status(400).json({ message: 'ID must be an integer' })
    }
};

async function mustBeInOrders(req, res, next) {
    const id = req.params.id;
    if (await inOrders(id)) {
        next()
    } else {
        res.status(404).json({ message: 'Order does not exist' })
    }
};

async function orderOwner(req, res, next) {
    const order = await Order.findByPk(req.params.id);
    if (order.userId === req.userId) {
        next()
    } else {
        res.status(400).json({ message: 'You are not the owner of this order' })
    }
};

async function productInProducts(req, res, next) {
    const { id, product_id } = req.params;
    const order = await orderController.getOrdersByID(id);
    const product = order.products.find(p => p.get('id') == product_id);
    if (product) {
        next();
    } else {
        res.status(400).json({ message: 'Product does not exist in the order' })
    }
};

async function productNotInProducts(req, res, next) {
    const { id, product_id } = req.params;
    const order = await orderController.getOrdersByID(id);
    const product = order.products.find(p => p.get('id') == product_id);
    if (!product) {
        next();
    } else {
        res.status(400).json({ message: 'Product already exist in the order' })
    }
};

async function stillOpen(req, res, next) {
    const order = await Order.findByPk(req.params.id);
    if (order.status === 'Pending') {
        next();
    } else {
        res.status(400).json({ message: `The order #${req.params.id} is already confirmed.` })
    }
};

module.exports = {
    checkFields,
    checkProducts,
    checkQuantity,
    mustBeInteger,
    mustBeInOrders,
    orderOwner,
    stillOpen,
    productInProducts,
    productNotInProducts,
    checkOneProduct
}