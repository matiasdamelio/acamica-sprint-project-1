const productController = require('../controllers/product.controller')
const db = require('../models');
const Product = db.product;
const { validPrice, isInteger } = require('../utils/utils');
// const { isInteger } = require('../utils/utils.products');

async function checkFields(req, res, next) {
    const { name, price } = req.body;

    if (name && price) {
        const product = await productController.getProduct(name)
        if (!product) {
            if (validPrice(parseFloat(price))) {
                next()
            } else {
                res.status(400).json({ message: 'Price must be numeric and greater than 0' })
            }
        } else {
            res.status(400).json({ message: 'Product already exists' })
        };
    } else {
        res.status(400).json({ message: 'One of the fields is empty or missing' })
    }
};

async function checkFieldsUpdate(req, res, next) {
    const { name, price } = req.body;
    const id = req.params.id;

    const productByID = await Product.findByPk(id)

    if (name || price) {
        if (name) {
            const product = await productController.getProduct(name)
            if (product && product.id != id) {
                return res.status(400).json({ message: 'Product name already exists' })
            }
        } else {
            req.body.name = productByID.name;
        }
        if (price) {
            if (!validPrice(parseFloat(price))) {
                return res.status(400).json({ message: 'Price must be numeric and greater than 0' })
            }
        } else {
            req.body.price = productByID.price;
        }
        next();
    } else {
        res.status(400).json({ message: 'At least one field must be complete' })
    }
};

function mustBeInteger(req, res, next) {
    const id = req.params.id;
    if (isInteger(id)) {
        next()
    } else {
        res.status(400).json({ message: 'ID must be an integer' })
    }
};

async function mustBeInProducts(req, res, next) {
    const id = req.params.id;
    const product = await Product.findByPk(id)
    if (product) {
        next()
    } else {
        res.status(404).json({ message: 'Product does not exist' })
    }
};

module.exports = {
    checkFields,
    checkFieldsUpdate,
    mustBeInteger,
    mustBeInProducts
};