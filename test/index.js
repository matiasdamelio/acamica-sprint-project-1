const chai = require('chai');
const expect = require('chai').expect;
const chaiHttp = require('chai-http');

chai.use(chaiHttp);
const url = 'http://localhost:3000';

describe('Signup of a new user', () => {
    it('Should insert a user', (done) => {
        chai.request(url)
            .post('/signup')
            .send({
                "username": "mocha_test",
                "password": "mocha_test",
                "fullname": "Mocha Test",
                "email": "mocha@test.com",
                "phone": "+54 (333) 333-3333",
                "address": "3333 Mocha Street"
              })
              .end( function(err, res) {
                console.log(res.body)
                expect(res).to.have.status(201);
                done();
              });
    });
});

describe('Signup of an existing user', () => {
    it('Should receive an error', (done) => {
        chai.request(url)
            .post('/signup')
            .send({
                "username": "test",
                "password": "test",
                "fullname": "Test Test",
                "email": "test@test.com",
                "phone": "+54 (222) 222-2222",
                "address": "2222 Test Street"
              })
              .end( function(err, res) {
                console.log(res.body)
                expect(res).to.have.status(400);
                done();
              });
    });
});